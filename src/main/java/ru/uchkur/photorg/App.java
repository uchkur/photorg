package ru.uchkur.photorg;
import java.io.File;
import java.util.*;

public class App 
{

    public static void main( String[] args )
    {
      Dir d = new Dir();
      Sockets s = new Sockets();
      d.registerCallback(s);
      d.walk(args[0]);
      s.show();
    }
}

class Sockets implements Dir.Callback
{
  private HashMap<Long, String> list;
  private HashMap<Long, String> sockets;
  private StringBuilder sb;
  private String s = new String ("");
  @Override
  public void callingBackD(File d)
  {
//todo    System.out.println(d.getPath());
  }

  @Override
  public void callingBackF(File f)
  {
      String ls = list.putIfAbsent(f.length(), f.getPath());
      if (ls!=null){
        sb.append(f.getPath()+"|");
        sb.append(ls);
        list.put(f.length(), sb.toString());
        sockets.put(f.length(), sb.toString());
        sb.setLength(0);
      };
  }

  public Sockets ()
  {
       this.sockets = new HashMap<Long, String>();
       this.list = new HashMap<Long, String>();
       this.sb = new StringBuilder(1024);
  }

  public void show()
  {
     for (Long key : sockets.keySet()) {
       System.out.println(key + " " + sockets.get(key));
     }
  }
}

class Dir 
{
    interface Callback {void callingBackF(File f);void callingBackD(File d);}
    Callback callback;
    public void registerCallback (Callback callback)
    {
      this.callback = callback;
    }

    public void walk (String dir)
    {
      File d = new File (dir);
      File[] f = d.listFiles();
      for (int i=0; i<f.length;i++)
      {
        if (f[i].exists()) 
          if (f[i].isDirectory())
          {
            callback.callingBackD(f[i]);
            walk(f[i].getPath());
          } else
          {
            callback.callingBackF(f[i]);
          }
      }
    }
}
